﻿using System;
using System.Net;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Windows;

namespace PrimerAppXamarin
{
    public partial class MainPage : ContentPage
    {
        int count = 0;
        public MainPage()
        {
            InitializeComponent();
        }
        void LlamarAPI(object sender, System.EventArgs e)
        {
            try
            {
                string url = $"https://api.openweathermap.org/data/2.5/weather?q={inputCiudad.Text}&appid=8dd9246a7151991d95abba121f568b18";
                Console.WriteLine(url);
                string respuesta = new WebClient().DownloadString(url);
                Models.Clases datosClima = JsonConvert.DeserializeObject<Models.Clases>(respuesta);

                labelCiudad.Text = $"\nCiudad: {datosClima.Name}";
                labelPais.Text = $"Pais: {datosClima.Sys.Country}";
                labelTemperatura.Text = $"Temperatura: {datosClima.Main.Temp - 273.15} C°";
                labelViento.Text = $"Viento: {datosClima.Wind.Speed} m/s";
                labelPresion.Text = $"Presion: {datosClima.Main.Pressure} hPa";
                labelHumedad.Text = $"Humedad: {datosClima.Main.Humidity}%";

            }
            catch (Exception ex)
            {
                Console.WriteLine("No se encontró una ciudad con el nombre ingresado. " + ex.Message);
                labelCiudad.Text = "La ciudad no existe.";
                labelPais.Text = null;
                labelTemperatura.Text = null;
                labelViento.Text = null;
                labelPresion.Text = null;
                labelHumedad.Text = null;
            }
        }
    }
}
