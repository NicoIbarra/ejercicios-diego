﻿using System;

namespace Ejercicio_Celular
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese una frase o palabra. Ingrese un punto para finalizar:");
            string inputString = Console.ReadLine();
            while (inputString != "."){
                ProcesarSecuencia.Convertir(inputString);
                Console.WriteLine("Ingrese una frase o palabra. Ingrese un punto para finalizar:");
                inputString = Console.ReadLine();
            }
        }
    }
}
