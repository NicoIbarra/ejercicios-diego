using System;

public class ProcesarSecuencia {

    public static string resultado = "";
    static char auxiliar = '\0';
    
    //Esta función recibe el string ingresado y lo convierte en un arreglo de caracteres.
    public static void Convertir(string inputString){
        Console.WriteLine("\nEl input ingresado fue: " + inputString);
        char[] arrChar = inputString.ToCharArray(0, inputString.Length);
        Procesar(arrChar);
    }

    //Esta función recibe el arreglo de caracteres de la función "Convertir".
    public static void Procesar(char[] secuencia){

        /* Evaluamos el caracter actual del arreglo de caracteres y lo procesamos. Antes se creaban todos los arreglos
        al principio de esta función, sin importar cuál se necesitaría en realidad. Ahora, dependiendo de la letra
        que se procese, se crea el arreglo necesario dentro del for. */
        for (int i = 0; i < secuencia.Length; i++){
            if(secuencia[i] == ' '){Calcular(secuencia[i], new char[]{' '}, '0');}
            if(secuencia[i] == 'a' || secuencia[i] == 'b' || secuencia[i] == 'c' ){Calcular(secuencia[i], new char[]{'a','b','c'}, '2');}
            if(secuencia[i] == 'd' || secuencia[i] == 'e' || secuencia[i] == 'f' ){Calcular(secuencia[i], new char[]{'d','e','f'}, '3');}
            if(secuencia[i] == 'g' || secuencia[i] == 'h' || secuencia[i] == 'i' ){Calcular(secuencia[i], new char[]{'g', 'h', 'i'}, '4');}
            if(secuencia[i] == 'j' || secuencia[i] == 'k' || secuencia[i] == 'l' ){Calcular(secuencia[i], new char[]{'j', 'k', 'l'}, '5');}
            if(secuencia[i] == 'm' || secuencia[i] == 'n' || secuencia[i] == 'o' ){Calcular(secuencia[i], new char[]{'m', 'n', 'o'}, '6');}
            if(secuencia[i] == 'p' || secuencia[i] == 'q' || secuencia[i] == 'r' || secuencia[i] == 's' ){Calcular(secuencia[i], new char[]{'p', 'q', 'r', 's'}, '7');}
            if(secuencia[i] == 't' || secuencia[i] == 'u' || secuencia[i] == 'v'){Calcular(secuencia[i], new char[]{'t', 'u', 'v'}, '8');}
            if(secuencia[i] == 'w' || secuencia[i] == 'x' || secuencia[i] == 'y' || secuencia[i] == 'z' ){Calcular(secuencia[i], new char[]{'w', 'x', 'y', 'z'}, '9');}   
        }

        MostrarResultado();
        
        /* Esta asignación reincia la variable "resultado" para que se puedan seguir ingresando palabras sin
        parar el programa. Para realizar los tests unitarios, hay que comentarla. */

        resultado = ""; 
    }

    /* Esta funcion recibe el caracter evaluado, el arreglo de caracteres al que pertenece dicho
    caracter, y la tecla que debería presionarse para escribirlo. */
    public static void Calcular (char valorActual, char[] numero, char tecla){
        int i = 0;
        
        /* Evaluamos el caracter actual con la posición actual de su arreglo. Por cada vez que
        no sean iguales, se deberá presionar la tecla para acceder a dicho caracter. */

        /* La variable auxiliar contiene el último caracter procesado. De esta forma
        diferenciamos la pausa que hace el usuario para ingresar caracteres de la misma tecla. */

        while(valorActual != numero[i]){
            if(auxiliar == tecla){
                resultado += " " + tecla;
                auxiliar = '\0';
            }else{
                resultado += tecla;
            }
            i++;
        }
        auxiliar = tecla;
        resultado += tecla; 
    }

    /* Esta función muestra los resultados obtenidos. */
    public static void MostrarResultado(){
        Console.WriteLine("La combinación de teclas para mostrar el string ingresado es: " + resultado);
    }
}