using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NetCoreTesting.Tests
{
    [TestClass]
    public class KeyboardTest
    {
        /* Las siguientes son pruebas para evaluar caracteres individuales. Para poder realizarlas hay que
        modificar los parámetros de la función Cacular_TestMethod y llamar a la funcion
        ProcesarSecuencia.Calcular() en la línea 27. */

        /* [DataRow('x', new char[]{'w', 'x', 'y', 'z'}, '9', "99")]
        [DataRow('b', new char[]{'a','b','c'}, '2', "22")] */

        /* Estas son pruebas para evaluar strings */
        [DataRow("hi", "44 444")]
        [DataRow("yes", "999337777")]
        [DataRow("foo bar", "333666 6660 022 2777")]
        [DataRow("hello world", "4433555 555666096667775553")]
        [DataRow("hola", "446665552")]
        
        [TestMethod]
        public void Calcular_TestMethod(string a, string expected)
        {
            //Act
                //Se asigna vacío a la variable "resultado" para evaluar los strings sin superponerlos.
                ProcesarSecuencia.resultado = "";
                ProcesarSecuencia.Convertir(a);

            //Assert
                //Se llama a la función "AreEqual" para comparar los resultados.
                Assert.AreEqual(expected, ProcesarSecuencia.resultado);
        }
    }
}
